import {useState} from 'react'
import Items from './Items'
import {v4 as uuidv4} from 'uuid'


export default function Forms() {

    const [arr, setArr] = useState([
        {txt :"promener le chien", id : uuidv4()},
        {txt :"apprendre react", id : uuidv4()},
        {txt :"sport", id : uuidv4()}
    ])

    const delFunc =id =>{
        console.log(id)
    }

    return (
        <div className="m-auto px-4 col-12 col-sm-10 col-lg-6">
            <form action="" className="mb-3">
                <label htmlFor="todo" className="form-label-mt-3">chose à faire</label>
                <input type="text" className="form-control" id="todo"/>
                <button className="btn btn-primary mt-2">envoyer</button>
            </form>
            <h2>Liste des chose à faire</h2>
            <ul className="list-group">
            {arr.map(item=>{
                    return(
                        <Items 
                        txt={item.txt}  
                        key={item.id} 
                        onClick = {delFunc}/>
                    )
                })}
              
            </ul>
        </div>
    )
}
